﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SymbolDrawing : MonoBehaviour 
{
	public enum SymbolsState {Unkwon, Drawing, Active}
	[SerializeField]
	private SymbolsState _state;
	public SymbolsState State
	{
		get
		{
			return _state;
		}

		set
		{
			_state = value;
			

			if (_state == SymbolsState.Drawing)
			{
				//SymbolImage.gameObject.SetActive(true);
				SymbolImage.enabled = true;
				ActivateColliders(true);
			}
			else if (_state == SymbolsState.Unkwon)
			{
				//SymbolImage.gameObject.SetActive(false);
				SymbolImage.enabled = false;
				ActivateColliders(false);
			}
		}

	}


	[SerializeField]
	List<Collider> _Colliders;

	[SerializeField]
	private int _ActiveColliders;


	public int colliderCount;

	public int tolerance = 1;

	public GameObject SymbolBarrier;
	public Image SymbolImage;


	// raycast parameters
	public int range = 30;
	public static RaycastHit hit;


	GameObject target;

	// Use this for initialization
	void Start () {
		_Colliders = new List<Collider>(transform.GetComponentsInChildren<Collider>());
		colliderCount = _Colliders.Count;
		_ActiveColliders = colliderCount;


	}

	void Update()
	{
		if (State == SymbolsState.Drawing)
		{
			if (Physics.Raycast(transform.position, transform.forward, out hit, range))
			{	
				target = hit.collider.gameObject;
			}

			if (Input.GetMouseButton(0))
			{
				if (target.tag == "DrawingCollider")
				{
					var rootSymbol = target.transform.root.GetComponent<SymbolDrawing>();
					if (rootSymbol)
					{
						rootSymbol.setCollider(target.GetComponent<Collider>());
					}
					//Destroy(target);
				}
			}
		}
	}

	public void setCollider(Collider collref, bool ActiveState = false)
	{
		if (_Colliders.Contains(collref))
		{
			collref.enabled = ActiveState;
			_ActiveColliders += (ActiveState?1:-1);
		}
		if (_ActiveColliders < tolerance)
		{
			Debug.Log("active Colliders");
			SymbolBarrier.SetActive(true);
			SymbolBarrier.GetComponent<SymbolCharge>().Recharge();
			SymbolImage.enabled = false;
			//triangleColliders.SetActive(false);	
			ActivateColliders(false);
		}

	}

	void ActivateColliders(bool ActiveState)
	{
		foreach (var item in _Colliders) 
		{
			item.gameObject.SetActive(ActiveState);
			item.enabled = true;
			_ActiveColliders = ActiveState ? _Colliders.Count : 0;
		}
	}




}
