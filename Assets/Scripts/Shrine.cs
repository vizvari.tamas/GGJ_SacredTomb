﻿using UnityEngine;
using System.Collections;

public class Shrine : MonoBehaviour 
{

	public EnemyController.EnemySymbol shrineSymbol;

	public int UnlockCharge;
	public int Charge;

	[SerializeField]
	private SymbolDrawing _DrawingRef;

	public static Shrine FindShrineFormSybol(EnemyController.EnemySymbol symbol)
	{
		var shrines = FindObjectsOfType<Shrine>();
		foreach(var shrine in shrines)
		{
			if (shrine.shrineSymbol == symbol)
			{
				return shrine;
			}
		}
		return null;
	}

	public void AddShrineCharge()
	{
		Charge++;
		if (Charge == UnlockCharge)
		{
			//unlock th
			_DrawingRef.gameObject.SetActive(true);
			_DrawingRef.State = SymbolDrawing.SymbolsState.Drawing;

		}

	}

}
