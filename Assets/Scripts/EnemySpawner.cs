﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct Wave
{
	public float WaveDelay;
	public List<GameObject> Enemies;
}


public class EnemySpawner : MonoBehaviour 
{

	public enum SpawnerStates { START, SPAWNING, WAITING, DONE } 
	[SerializeField]
	private SpawnerStates _state;
	public SpawnerStates State
	{
		get
		{
			return _state;
		}
		set
		{
			_state = value;

			if (_state == SpawnerStates.SPAWNING)
			{
				Debug.Log("Swet spawner state");
				//spawn the next wave
				this.SpawnEnemies();
			}
		}

	}

	public List<Wave> Waves;
	public List<Transform> SpawnPoints;
	public int CurrentWave = 0;
	public List<GameObject> SpawnedEnemies;

	// Use this for initialization
	void Start () 
	{
		if (SpawnPoints.Count == 0)
		{
			foreach ( Transform t in transform)
			{
				SpawnPoints.Add(t);
			}

		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void SpawnEnemies()
	{

		StartCoroutine(DelayedSpawnEnemies());

	}

	IEnumerator DelayedSpawnEnemies()
	{
		if (Waves.Count > CurrentWave)
		{
			
			yield return new WaitForSeconds(Waves[CurrentWave].WaveDelay);

			// normal spawn
			foreach (var enemyPrefab in Waves[CurrentWave].Enemies)
			{
				int randomSpawnPoint = Random.Range(0, SpawnPoints.Count);
				var Enemy = Instantiate(enemyPrefab, SpawnPoints[randomSpawnPoint].position, Quaternion.identity) as GameObject; 
				
				SpawnedEnemies.Add(Enemy);
				
				EnemyController controller = Enemy.GetComponent<EnemyController>();
				controller.OnEnemyDeath += EnemyDeathHandler;
				
				
				
			}
			
			// after spawning a wave, set to wa
			CurrentWave++;
			State = SpawnerStates.WAITING;
		}
		else
		{
			// TODO: Endless mode, we simply reset the spawner for now
			CurrentWave = 0;
			SpawnEnemies();
			
		}
		
		
		
		//		if (!spawnedW2)
		//		{
		//			for (int i = 0; i < enemyCount; i++)
		//			{
		//				int sp1 = Random.Range(1, 10);
		//				Instantiate(enemyRed, spawnPoints[i+1].transform.position, new Quaternion(0, 0, 0, 0));
		//			}
		//			spawnedW2 = true;
		//		}

	}



	void EnemyDeathHandler(EnemyController source)
	{
		Debug.Log("Enemy death handler");
		source.OnEnemyDeath -= EnemyDeathHandler;

		//remove the enemy from the spawned list
		SpawnedEnemies.Remove(source.gameObject);

		// if all enemies are dead, we can spawn the next wave
		if (SpawnedEnemies.Count == 0)
		{
			_state = SpawnerStates.SPAWNING;
			this.SpawnEnemies();
		}

		//add shrine charge
		var shrineRef = Shrine.FindShrineFormSybol(source.enemySymbol);
		if (shrineRef != null)
		{
			shrineRef.AddShrineCharge();
		}
	}

}
